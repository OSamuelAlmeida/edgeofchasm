﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

	public Transform player;

	private Transform transform;

	// Use this for initialization
	void Start () {
		transform = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		transform.SetPositionAndRotation (new Vector3 (player.position.x, transform.position.y, transform.position.z), transform.rotation);
	}
}
