﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonShoot : MonoBehaviour {

	public float acceleration;
	public Animator gateAnimator;

	public Transform enemyPoint;
	public GameObject enemy;

	bool playerIn;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (playerIn) {
			if (Input.GetButtonDown("Interact") && PlayerInventory.item == "Ball") {
				PlayerInventory.item = "";

				if (acceleration >= 10f) {
					gateAnimator.SetTrigger ("Open");

					if (acceleration >= 15f) {
						GameObject.Instantiate (enemy, enemyPoint);
					}
				}
			}
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.CompareTag ("Player")) {
			playerIn = true;
		}
	}

	void OnTriggerExit (Collider other) {
		playerIn = false;
	}
}
