﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour {

	public float walkingSpeed;
	public float runningSpeed;

	private float speed;
	private Animator animator;

	private Transform transform;

	private Vector3 initialScale;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();

		transform = GetComponent<Transform> ();

		initialScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Fire3")) {
			speed = runningSpeed;
			animator.SetBool ("isRunning", true);
		} else {
			speed = walkingSpeed;
			animator.SetBool ("isRunning", false);
		}
			

		if (Input.GetButtonDown ("Jump")) {
			animator.SetTrigger ("isJumping");
		}

		if (Input.GetButtonDown ("Interact")) {
			animator.SetTrigger ("isInteracting");
		}

		float translation = Input.GetAxis ("Horizontal") * speed * Time.deltaTime;

		transform.Translate (0, 0, translation);

		if (translation != 0 && speed == walkingSpeed) {
			animator.SetBool ("isWalking", true);
		} else {
			animator.SetBool ("isWalking", false);
		}

		if (translation < 0) {
			transform.localScale = new Vector3 (initialScale.x, initialScale.y, -initialScale.z);
		} else if (translation > 0) {
			transform.localScale = new Vector3 (initialScale.x, initialScale.y, initialScale.z);
		}
	}
}
