﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectBall : MonoBehaviour {

	private bool ballDetected;
	private GameObject ball;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (ballDetected) {
			if (Input.GetButtonDown ("Interact")) {
				PlayerInventory.item = "Ball";
				Destroy (ball);
			}
		}
	}

	void OnTriggerEnter (Collider other) {
		if (other.gameObject.CompareTag ("Ball")) {
			if (PlayerInventory.item == "") {
				ballDetected = true;
				ball = other.gameObject;
			}
		}
	}

	void OnTriggerExit (Collider other) {
		ballDetected = false;
	}
}
